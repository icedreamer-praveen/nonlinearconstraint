import numpy as np
from scipy.optimize import minimize, NonlinearConstraint

# Define your features (40 values) and upper and lower bounds for the weighted sum
features = np.array([1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0, 30.0, 31.0, 32.0, 33.0, 34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0])
lower_bound = 4  # Define your lower bound constraint
upper_bound = 4  # Define your upper bound constraint

# Objective Function: Weights
def objective_function(weights):
    return np.sum(weights)  # We want to maximize the sum of weights

# Constraints
def constraint_weights_total(weights):
    return np.sum(weights) - 100.0  # The sum of weights must be 100

def constraint_weight_bounds(weights):
    return 2.5 - weights  # Each weight must be <= 2.5

def constraint_feature_weighted_sum(weights):
    weighted_sum = np.dot(weights, features)
    return weighted_sum - upper_bound  # The weighted sum of features must be <= upper bound

# Define the constraints
constraints = (
    NonlinearConstraint(constraint_weights_total, 0, 0),
    NonlinearConstraint(constraint_weight_bounds, -np.inf, 0),
    NonlinearConstraint(constraint_feature_weighted_sum, -np.inf, 0)
)

# Initial guess for weights (you can start with any initial values within the bounds)
initial_guess = np.ones(40)

# Optimization
result = minimize(objective_function, initial_guess, constraints=constraints, method='trust-constr')

print(result.x.dot(features))