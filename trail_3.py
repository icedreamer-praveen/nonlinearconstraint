import pandas as pd
import numpy as np
from scipy.optimize import minimize, NonlinearConstraint


raw_data = pd.read_csv('/home/prabin/Documents/NonLinearConstraint/data.csv')

raw_data['Index No'] = raw_data['Index No'].replace(to_replace='NR',value=np.nan).astype(float)
raw_data['Index No'].fillna(raw_data['Index No'].mean(), inplace=True)

raw_data.head()


# Define your features
num_features = 2
num_weights = len(raw_data)
features= raw_data.iloc[:,1:3].T.values

# Define upper and lower bounds for each feature's weighted sum
lower_bounds = [3.6, 3.9]  # Lower bounds for features
upper_bounds = [4.4, 4.8]  # Upper bounds for features

# Objective Function: Weights
def objective_function(weights):
    return -np.sum(weights)  # We want to maximize the sum of weights, so we negate it

# Constraints
def constraint_weights_total(weights):
    return np.sum(weights) - 100.0  # The sum of weights must be 100

def constraint_weight_bounds(weights):
    return 2.5 - weights  # Each weight must be <= 2.5

def constraint_feature_weighted_sum(weights):
    constraints = []
    for i in range(num_features):
        weighted_sums = np.dot(weights, features[i])
        constraints.extend([weighted_sums - upper_bounds[i], lower_bounds[i] - weighted_sums])
    return constraints

# Define the constraints
constraints = (
    NonlinearConstraint(constraint_weights_total, 0, 0),
    NonlinearConstraint(constraint_weight_bounds, -np.inf, 0),
    NonlinearConstraint(constraint_feature_weighted_sum, 0, 0)
)

# Initial guess for weights (you can start with any initial values within the bounds)
initial_guess = np.random.randn(num_weights)

# Optimization
result = minimize(objective_function, initial_guess, constraints=constraints, method='trust-constr',) #   options = {"maxiter":10000}

print(result.x.dot(features[0]))
print(result.x.dot(features[1]))



# Define your features
num_features = 2
num_weights = len(raw_data)
features= raw_data.iloc[:,1:3].T.values

# Define upper and lower bounds for each feature's weighted sum
lower_bounds = [3.6, 3.9]  # Lower bounds for features
upper_bounds = [4.4, 4.8]  # Upper bounds for features

target_values = [4, 4.35]

# Objective Function: Loss Function
def objective_function(weights):
    # Calculate the weighted sums for each feature
    weighted_sums = np.zeros(num_features)
    for i in range(num_features):
        weighted_sums[i] = np.sum(weights * features[i])

    # Calculate the loss as the absolute difference from the target values
    loss = np.abs(np.sum(weighted_sums) - np.sum(target_values))
    # print(loss)
    return loss

# Constraints
def constraint_weights_total(weights):
    return np.sum(weights) - 100.0  # The sum of weights must be 100

def constraint_weight_bounds(weights):
    return 2.5 - weights  # Each weight must be <= 2.5

# Define the constraints
constraints = (
    NonlinearConstraint(constraint_weights_total, 0, 0),
    NonlinearConstraint(constraint_weight_bounds, -np.inf, 0)
)

# Initial guess for weights (you can start with any initial values within the bounds)
initial_guess = np.random.randn(num_weights).astype(float)

# Optimization
result = minimize(objective_function, initial_guess, constraints=constraints, method='trust-constr')

print(result.x.dot(features[0]))
print(result.x.dot(features[1]))